
"""env.py: Environment configuration for the maze simulation"""

__author__ = "Pablo Alvarado"
__copyright__ = "Copyright 2020, Pablo Alvarado"
__license__ = "BSD 3-Clause License (Revised)"


import random
import maze
import agent


class Environment:
    """"Environment class

        The environment holds a maze and one or more agents

    """

    def __init__(self, nx, ny, cellSizeX, cellSizeY=None):
        if cellSizeY is None:
            cellSizeY = cellSizeX
        self.maze = maze.Maze(nx, ny, cellSizeX, cellSizeY)
        self.agent = agent.Agent()
        self.reset()

    def reset(self):
        self.agent.state.reset()
        self.agent.setPos((self.maze.startX+0.5)*self.maze.cellSizeX,
                          (self.maze.startY+0.5)*self.maze.cellSizeY)

    def tryAction(self, anAgent, action, arg=None):
        """Executes the action on the agent, but verifies that the achieved
           result is compatible with the environment.  The environment
           fixes all invalid information
        """

        if arg is None:
            pre = action()
        else:
            pre = action(arg)

        anAgent.setState(self.validate(anAgent, pre))

    def validate(self, anAgent, newState):
        """Restrict the newState to a valid environmental compatible
           state
        """

        res = agent.AgentState(newState)

        # Is it rotating?
        if (not anAgent.state.sameAngle(newState)):
            # For now, let's asume no major obstacles for rotation,
            # but some noise
            res.angle += random.normalvariate(0, anAgent.rotationNoise)

        # Is it moving?
        if (not anAgent.state.samePos(newState)):
            length = anAgent.state.distance(newState)
            nx = random.normalvariate(0, length*anAgent.translationNoiseFactor)
            ny = random.normalvariate(0, length*anAgent.translationNoiseFactor)

            dx, dy = self.maze.tryMovement(anAgent.state.posX,
                                           anAgent.state.posY,
                                           newState.posX+nx,
                                           newState.posY+ny,
                                           anAgent.radius)

            res.posX, res.posY = dx, dy

        return res

# -*- coding: utf-8 -*-

""" All GUI related classes, include some code autogenerated by PyQt  """

import math
from PyQt5 import QtCore, QtGui, QtWidgets
import simloop
import mazedlg


class MazeView(QtWidgets.QGraphicsView):
    """Specialized Qt graphics view object to hold the maze

       This overloading takes care of the mouse controled zoom and scrolling
    """

    def __init__(self, parent=None):
        super(MazeView, self).__init__(parent)
        self.parent = parent
        self.zoom = 1
        self.prevMousePos = None

    def wheelEvent(self, event):
        super(MazeView, self).wheelEvent(event)
        factor = pow(2, 1/10)
        if (event.angleDelta().x() > 0 or event.angleDelta().y() > 0):
            self.zoom = min(self.zoom*factor, 32)
        elif (event.angleDelta().x() < 0 or event.angleDelta().y() < 0):
            self.zoom = max(0.125, self.zoom/factor)

        self.resetTransform()
        self.scale(self.zoom, self.zoom)

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:  # or Qt.MiddleButton
            self.prevMousePos = event.pos()
        else:
            super(MazeView, self).mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            offset = self.prevMousePos - event.pos()
            self.prevMousePos = event.pos()

            self.verticalScrollBar().setValue(
                self.verticalScrollBar().value() + offset.y())
            self.horizontalScrollBar().setValue(
                self.horizontalScrollBar().value() + offset.x())
        else:
            super(MazeView, self).mouseMoveEvent(event)


class GraphicsAgentItem(QtWidgets.QGraphicsItem):
    """Custom agent item.

       It draws a circle with an arrow showing the agent's orientation
    """

    def __init__(self, agent, parent=None):
        super(GraphicsAgentItem, self).__init__(parent)
        # self.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)

        self.state = agent.state
        self.radius = agent.radius

        # Pen and brush for the body
        self.pen = QtGui.QPen(QtGui.QColor(agent.color[0],
                                           agent.color[1],
                                           agent.color[2],
                                           255),
                              1)
        self.brush = QtGui.QBrush(self.pen.color())

        # Pen and blush for the arrow
        self.apen = QtGui.QPen(QtGui.QColor(255, 255, 192, 255), 1)
        self.abrush = QtGui.QBrush(QtCore.Qt.NoBrush)

    def boundingRect(self):
        r = math.ceil(self.radius)
        return QtCore.QRectF(-r, -r, 2*r+1, 2*r+1)

    def paint(self, painter, option, widget):

        self.setPos(self.state.posX, self.state.posY)

        c = math.cos(math.radians(self.state.angle))
        s = math.sin(math.radians(self.state.angle))
        r = self.radius

        painter.setPen(self.pen)
        painter.setBrush(self.brush)
        painter.drawEllipse(QtCore.QPointF(0.0, 0.0), r, r)

        painter.setPen(self.apen)
        painter.setBrush(self.abrush)

        painter.drawLine(-r*c, -r*s, +r*c, +r*s)
        painter.drawLine(-r*s, +r*c, +r*c, +r*s)
        painter.drawLine(+r*s, -r*c, +r*c, +r*s)

    def shape(self):
        path = QtGui.QPainterPath()
        path.addEllipse(self.boundingRect())
        return path

    def setState(self, state):
        self.state.copy(state)
        self.setPos(self.state.posX, self.state.posY)


class Ui_MainWindow(QtCore.QObject):
    """ Main GUI Window """

    renderSignal = QtCore.pyqtSignal()

    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        self.mazeSeed = 1
        self.simLoop = simloop.SimLoop()
        self.agentItem = None
        self.renderSignal.connect(self.render)

    def startThreads(self):
        """ Initialize the simulation threads"""
        self.threadPool = QtCore.QThreadPool()
        self.threadPool.start(self.simLoop.timer)
        self.threadPool.start(self.simLoop)

    def setupUi(self, MainWindow):
        """Build the whole GUI window"""

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1024, 768)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.mainLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.mainLayout.setObjectName("mainLayout")

        self.canvas = MazeView(self.centralwidget)
        self.canvas.setObjectName("canvas")

        self.mainLayout.addWidget(self.canvas, 0, 0, 1, 1)
        self.buttonsBar = QtWidgets.QHBoxLayout()
        self.buttonsBar.setObjectName("buttonsBar")

        self.playButton = QtWidgets.QPushButton(self.centralwidget)
        self.playButton.setObjectName("playButton")
        self.playButton.clicked.connect(self.playClicked)
        self.buttonsBar.addWidget(self.playButton)

        self.pauseButton = QtWidgets.QPushButton(self.centralwidget)
        self.pauseButton.setObjectName("pauseButton")
        self.pauseButton.clicked.connect(self.pauseClicked)
        self.pauseButton.setEnabled(False)

        self.buttonsBar.addWidget(self.pauseButton)

        self.stepButton = QtWidgets.QPushButton(self.centralwidget)
        self.stepButton.setObjectName("stepButton")
        self.stepButton.clicked.connect(self.stepClicked)
        self.buttonsBar.addWidget(self.stepButton)

        self.stopButton = QtWidgets.QPushButton(self.centralwidget)
        self.stopButton.setObjectName("stopButton")
        self.stopButton.clicked.connect(self.stopClicked)
        self.stopButton.setEnabled(False)

        self.buttonsBar.addWidget(self.stopButton)

        self.restartButton = QtWidgets.QPushButton(self.centralwidget)
        self.restartButton.setObjectName("restartButton")
        self.restartButton.clicked.connect(self.restartClicked)
        self.buttonsBar.addWidget(self.restartButton)

        spacerItem = QtWidgets.QSpacerItem(40, 20,
                                           QtWidgets.QSizePolicy.Minimum,
                                           QtWidgets.QSizePolicy.Minimum)

        self.buttonsBar.addItem(spacerItem)
        self.speedSlider = QtWidgets.QSlider(self.centralwidget)
        self.speedSlider.setOrientation(QtCore.Qt.Horizontal)
        self.speedSlider.setObjectName("speedSlider")
        self.speedSlider.setRange(0, 2000)
        self.speedSlider.setValue(200)
        self.speedSlider.valueChanged.connect(self.speedChanged)
        self.buttonsBar.addWidget(self.speedSlider)
        self.mainLayout.addLayout(self.buttonsBar, 1, 0, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1024, 30))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuMaze = QtWidgets.QMenu(self.menubar)
        self.menuMaze.setObjectName("menuMaze")
        self.menuAgent = QtWidgets.QMenu(self.menubar)
        self.menuAgent.setObjectName("menuAgent")

        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")

        MainWindow.setStatusBar(self.statusbar)

        self.actionLoad_maze = QtWidgets.QAction(MainWindow)
        self.actionLoad_maze.setObjectName("actionLoad_maze")

        self.actionSave_maze = QtWidgets.QAction(MainWindow)
        self.actionSave_maze.setObjectName("actionSave_maze")

        self.actionQuit = QtWidgets.QAction(MainWindow)
        self.actionQuit.setObjectName("actionQuit")
        self.actionQuit.triggered.connect(self.quit)

        self.mazeConfigDialog = QtWidgets.QDialog(MainWindow)
        self.mazeConfigDialog.ui = mazedlg.MazeDialog(self.mazeConfigDialog)
        self.mazeConfigDialog.ui.setupUi(self.mazeConfigDialog)
        self.mazeConfigDialog.ui.applySignal.connect(self.onBuildNewMaze)

        self.actionConfigMaze = QtWidgets.QAction(MainWindow)
        self.actionConfigMaze.setObjectName("actionConfigMaze")
        self.actionConfigMaze.triggered.connect(self.showMazeConfig)

        self.actionBuild_new = QtWidgets.QAction(MainWindow)
        self.actionBuild_new.setObjectName("actionBuild_new")
        self.actionBuild_new.triggered.connect(self.onBuildNewMaze)

        self.actionLoad_environment = QtWidgets.QAction(MainWindow)
        self.actionLoad_environment.setObjectName("actionLoad_environment")

        self.actionSave_environment = QtWidgets.QAction(MainWindow)
        self.actionSave_environment.setObjectName("actionSave_environment")

        self.actionSimple = QtWidgets.QAction(MainWindow)
        self.actionSimple.setObjectName("actionSimple")

        self.actionContinous = QtWidgets.QAction(MainWindow)
        self.actionContinous.setObjectName("actionContinous")

        self.menuFile.addAction(self.actionLoad_environment)
        self.menuFile.addAction(self.actionSave_environment)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionQuit)
        self.menuMaze.addAction(self.actionConfigMaze)
        self.menuMaze.addAction(self.actionBuild_new)
        self.menuMaze.addSeparator()
        self.menuMaze.addAction(self.actionLoad_maze)
        self.menuMaze.addAction(self.actionSave_maze)
        self.menuAgent.addAction(self.actionSimple)
        self.menuAgent.addAction(self.actionContinous)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuMaze.menuAction())
        self.menubar.addAction(self.menuAgent.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.playButton.setText(_translate("MainWindow", "Play"))
        self.pauseButton.setText(_translate("MainWindow", "Pause"))
        self.stepButton.setText(_translate("MainWindow", "Step"))
        self.stopButton.setText(_translate("MainWindow", "Stop"))
        self.restartButton.setText(_translate("MainWindow", "Restart"))
        self.menuFile.setTitle(_translate("MainWindow", "Fi&le"))
        self.menuMaze.setTitle(_translate("MainWindow", "&Maze"))
        self.menuAgent.setTitle(_translate("MainWindow", "A&gent"))
        self.actionLoad_maze.setText(_translate("MainWindow", "&Load maze"))
        self.actionSave_maze.setText(_translate("MainWindow", "&Save maze"))
        self.actionSave_maze.setShortcut(_translate("MainWindow",
                                                    "Ctrl+Shift+S"))
        self.actionQuit.setText(_translate("MainWindow", "&Quit"))
        self.actionQuit.setShortcut(_translate("MainWindow", "Ctrl+X"))
        self.actionConfigMaze.setText(_translate("MainWindow",
                                                 "&Configure maze"))
        self.actionBuild_new.setText(_translate("MainWindow", "&Build new"))
        self.actionLoad_environment.setText(_translate("MainWindow",
                                                       "&Load environment"))
        self.actionSave_environment.setText(_translate("MainWindow",
                                                       "&Save environment"))
        self.actionSave_environment.setShortcut(_translate("MainWindow",
                                                           "Ctrl+S"))
        self.actionSimple.setText(_translate("MainWindow", "&Simple"))
        self.actionContinous.setText(_translate("MainWindow", "&Continous"))

    def setEnvironment(self, env):
        self.env = env
        self.onBuildNewMaze()

    def setStepper(self, stepper):
        self.simLoop.setStepper(stepper)

    def stepper(self):
        return self.simLoop.stepper

    def onBuildNewMaze(self):
        self.env.maze.build(self.mazeSeed)
        self.env.reset()

        self.mazeToScene(self.env.maze)
        self.canvas.setScene(self.scene)

        print(self.env.agent)

        # DBG: Add a red circle for the (0,0) cell
        self.agentItem = GraphicsAgentItem(self.env.agent)
        self.scene.addItem(self.agentItem)

        self.canvas.show()
        self.fitInCanvasView()

        self.mazeSeed += 1

    def fitInCanvasView(self):

        self.canvas.fitInView(self.scene.sceneRect(),
                              QtCore.Qt.KeepAspectRatio)
        mat = self.canvas.transform()
        self.canvas.zoom = math.sqrt(mat.m11()*mat.m11() + mat.m12()*mat.m12())
        if self.canvas.zoom < 1:
            self.canvas.zoom = 1
            self.canvas.resetTransform()

    def mazeToScene(self, maze):
        """Build the QGraphicsScene with the maze."""

        # Height and width of the maze image (excluding padding), in pixels
        height = maze.ny*maze.cellSizeY + 1
        width = maze.nx*maze.cellSizeX + 1

        self.scene = QtWidgets.QGraphicsScene(0, 0, width, height)

        # Scaling factors mapping maze coordinates to image coordinates
        scy, scx = maze.cellSizeY, maze.cellSizeX

        startBrush = QtGui.QBrush(QtGui.QColor(192, 255, 192, 64))
        startPen = QtGui.QPen(QtGui.QColor(192, 255, 192, 128), 0)

        self.scene.addRect(maze.startX*maze.cellSizeX,
                           maze.startY*maze.cellSizeY,
                           maze.cellSizeX,
                           maze.cellSizeY,
                           startPen,
                           startBrush)

        endBrush = QtGui.QBrush(QtGui.QColor(192, 192, 255, 64))
        endPen = QtGui.QPen(QtGui.QColor(192, 192, 255, 128), 0)

        self.scene.addRect(maze.endX*maze.cellSizeX,
                           maze.endY*maze.cellSizeY,
                           maze.cellSizeX,
                           maze.cellSizeY,
                           endPen,
                           endBrush)

        pen = QtGui.QPen(QtGui.QColor(0, 0, 64, 255), 1)

        # Draw the "South" and "East" walls of each cell, if present (these
        # are the "North" and "West" walls of a neighbouring cell in
        # general, of course).
        for y in range(maze.ny):
            for x in range(maze.nx):
                if maze.cellAt(x, y).walls['S']:
                    x1, y1, x2, y2 = x*scx, (y+1)*scy, (x+1)*scx, (y+1)*scy
                    self.scene.addLine(x1, y1, x2, y2, pen)
                if maze.cellAt(x, y).walls['E']:
                    x1, y1, x2, y2 = (x+1)*scx, y*scy, (x+1)*scx, (y+1)*scy
                    self.scene.addLine(x1, y1, x2, y2, pen)

        # Draw the North and West maze border, which won't have been drawn
        # by the procedure above.
        self.scene.addLine(0, 0, 0, height-1, pen)
        self.scene.addLine(0, 0, width-1, 0, pen)

    def playClicked(self):
        self.playButton.setEnabled(False)
        self.stepButton.setEnabled(False)
        self.pauseButton.setEnabled(True)
        self.stopButton.setEnabled(True)

        self.simLoop.play()

    def pauseClicked(self):
        self.pauseButton.setEnabled(False)
        self.playButton.setEnabled(True)
        self.stepButton.setEnabled(True)
        self.stopButton.setEnabled(True)

        self.simLoop.pause()

    def stepClicked(self):
        self.pauseButton.setEnabled(False)
        self.playButton.setEnabled(True)
        self.stepButton.setEnabled(True)
        self.stopButton.setEnabled(True)

        self.simLoop.step()

    def stopClicked(self):
        self.pauseButton.setEnabled(False)
        self.playButton.setEnabled(True)
        self.stepButton.setEnabled(True)
        self.stopButton.setEnabled(False)

        self.simLoop.stop()

    def restartClicked(self):
        self.playButton.setEnabled(False)
        self.stepButton.setEnabled(False)
        self.pauseButton.setEnabled(True)
        self.stopButton.setEnabled(True)

        self.simLoop.restart()

    def speedChanged(self, value):
        # print("[DBG] Setting new interval to ", value, "ms")
        self.simLoop.setInterval(value/1000.0)
        self.statusbar.showMessage("Interval set to {}ms".format(value),
                                   1000)

    def render(self):
        """Called only within the GUI thread to trigger an environment
           rendering.

           To enforce another thread into calling this method, just
            emit the signal renderSignal in this gui
        """

        # print("[DBG] gui.Ui_MainWindow.render(): we got called by a signal?")
        if self.agentItem is not None:
            self.agentItem.setState(self.env.agent.state)
            self.agentItem.update()

    def quit(self, arg=None):
        self.simLoop.timer.quit()
        self.simLoop.quit()
        self.threadPool.waitForDone(1000)  # Wait 1s for everything to end
        QtCore.QCoreApplication.quit()

    def __del__(self):
        self.simLoop.timer.quit()
        self.simLoop.quit()

    def closeEvent(self, event):
        quit(self, event)

    def showMazeConfig(self):
        self.mazeConfigDialog.ui.setValues(self.env.maze)
        self.mazeConfigDialog.show()

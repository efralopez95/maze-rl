
"""example.py: Use a maze and solve it with classic wall-following
   algorithm.

   The RL versions should be faster
"""

__author__ = "Pablo Alvarado"
__copyright__ = "Copyright 2020, Pablo Alvarado"
__license__ = "BSD 3-Clause License (Revised)"

import random
import dispatcher
import env


class ExampleStepper:
    """Simple stepper performing a random walk"""

    def __init__(self, dispatch):
        self.dispatch = dispatch
        self.env = self.dispatch.env()
        self.init()

    def init(self):
        """Reset and setup the stepper"""

        print("ExampleStepper.init() called")

        self.env.reset()

        step = self.env.maze.cellSizeX/3

        # Let chance peek most of the time an advance action
        self.actions = [self.env.agent.right,
                        self.env.agent.left,
                        lambda: self.env.agent.advance(step),
                        lambda: self.env.agent.advance(step),
                        lambda: self.env.agent.advance(step),
                        lambda: self.env.agent.advance(step)]

    def step(self, iteration):
        """Perform one simulation step"""

        print("ExampleStepper.step(", iteration, ")")

        self.env.tryAction(self.env.agent, random.choice(self.actions))
        print("               Agent after last step: ", self.env.agent)

        self.dispatch.render()


# #######################
# # Main control center #
# #######################

# This object centralizes everything
theDispatcher = dispatcher.Dispatcher()

# Give it an environment (maze + agent)
theDispatcher.setEnvironment(env.Environment(12, 8, 15))

# Give it also the simulation stepper, which needs access to the
# agent and maze in the dispatcher.
theDispatcher.setStepper(ExampleStepper(theDispatcher))

# Start the GUI and run it until quit is selected
# (Remember Ctrl+\ forces python to quit, in case it is necessary)
theDispatcher.run()
